

set encoding=utf-8
"scriptencoding utf-8

set fileencoding=utf-8
set fileencodings=ucs-boms,utf-8,euc-jp,cp932
set fileformats=unix,dos,mac


set number
set wrap

"For Go
set noexpandtab
set tabstop=4
set shiftwidth=4
set autoindent

set noswapfile
set nobackup
set noundofile

set clipboard=unnamed
set shortmess+=I

"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/home/kenya888/.cache/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/home/kenya888/.cache/dein')
  call dein#begin('/home/kenya888/.cache/dein')

  " Add or remove your plugins here like this:
  let s:toml = '~/.config/nvim/plugins.toml'
  let s:lazy_toml = '~/.config/nvim/lazy_plugins.toml'

  call dein#load_toml(s:toml, {'lazy': 0})
  call dein#load_toml(s:lazy_toml, {'lazy': 1})

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
"if dein#check_install()
"  call dein#install()
"endif

"End dein Scripts-------------------------

let g:deoplete#enable_at_startup=1
